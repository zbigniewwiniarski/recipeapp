import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe';

@Injectable() 
export class RecipeService {
    private recipes: Array<Recipe> = [
        { id: 1, name: 'Solyanka', bigAmount: true, ingridients:[
        { foodType: "Lamb", amount: 20, unit: "kg", cookingMethods: [
            "barbecue", "boil"
        ]}
        ] },
        { id: 2, name: 'Haggis', bigAmount: false, ingridients:[
        { foodType: "juice", amount: 20, unit: "ml", cookingMethods: [
            "boil", "steam"
        ]},
        { foodType: "Pork", amount: 546, unit: "pieces", cookingMethods: [
            "fry", "chop"
        ]}
        ] },
        { id: 3, name: 'Okonomiyaki', bigAmount: true, ingridients:[] }
    ];

    constructor() {      
    }

    getRecipes(): Array<Recipe> {
        return this.recipes;
    }

    addRecipe(newRecipe: Recipe) {
        this.recipes.push(newRecipe);
    }
}