import { Ingridient } from './ingridient';

export class Recipe {
  id: number;
  name: string;
  bigAmount: boolean;
  ingridients: Array<Ingridient>;
}