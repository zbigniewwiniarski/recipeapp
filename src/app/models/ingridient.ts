export class Ingridient {
    foodType: string;
    amount: number;
    unit: string;
    cookingMethods: Array<string>;
}