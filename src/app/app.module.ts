import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './components/appRoot/app-root.component';
import { RecipeDetailsComponent } from './components/recipeDetails/recipe-details.component';
import { RecipeFormComponent } from './components/recipeForm/recipe-form.component';
import { AddIngridientFormComponent } from './components/addIngridientForm/add-ingridient-form.component';

import { RecipeService } from './services/recipeService';

@NgModule({
  declarations: [
    AppComponent,
    RecipeDetailsComponent,
    RecipeFormComponent,
    AddIngridientFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    RecipeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
