import { Component, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../../models/recipe';

@Component({
  selector: 'recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.css']
})
export class RecipeFormComponent {
  @Output() recipeCreated = new EventEmitter();
  newRecipe: Recipe = new Recipe();
  active: boolean = true;

  onSubmit() {
    this.recipeCreated.emit({ recipe: this.newRecipe });

    this.newRecipe = new Recipe();
    this.active = false;
    setTimeout(() => this.active = true, 0);
  }
}