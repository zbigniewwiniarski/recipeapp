import { Component, OnInit } from '@angular/core';
import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipeService'

@Component({
  selector: 'app-root',
  templateUrl: './app-root.component.html',
  styleUrls: ['./app-root.component.css'],
  providers: [RecipeService]
})
export class AppComponent implements OnInit {
  recipes: Recipe[];
  activeRecipe: Recipe;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
     this.recipes = this.recipeService.getRecipes();
  }

  selectRecipe(recipe) {
    this.activeRecipe = recipe;
  }

  onRecipeCreated(event) {
    this.recipes.push(event.recipe);
  }
}
