import { Component, Input, OnChanges } from '@angular/core';
import { Ingridient } from '../../models/ingridient';
import { Recipe } from '../../models/recipe';

@Component({
  selector: 'add-ingridient-form',
  templateUrl: './add-ingridient-form.component.html',
  styleUrls: ['./add-ingridient-form.component.css']
})
export class AddIngridientFormComponent {
  @Input() recipe: Recipe;
  newIngridient: Ingridient = new Ingridient();
  active: boolean = true;

  foodTypes: Array<string> = ['Pork', 'Fish', 'Lamb', 'Soya']
  units: Array<string> = ['ml', 'g', 'tsp', 'wgl'];
  cookingMethods: Array<string> = ['bake', 'boil', 'cook', 'chop'];

  onSubmit() {
    this.recipe.ingridients.push(this.newIngridient);
    this.newIngridient = new Ingridient();
  }
}